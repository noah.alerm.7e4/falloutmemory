package com.example.falloutmemory

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

/**
 * This game mode implements the hardcore mode and the count down extra functionalities, as well as
 * the pause button.
 */
class GameDeathclawActivity : AppCompatActivity(), View.OnClickListener {
    //VARIABLES
    //layout
    private lateinit var pauseButton: ImageView
    private lateinit var chronometer: Chronometer
    private lateinit var moveCounter: TextView
    private lateinit var card1: ImageView
    private lateinit var card2: ImageView
    private lateinit var card3: ImageView
    private lateinit var card4: ImageView
    private lateinit var card5: ImageView
    private lateinit var card6: ImageView
    private lateinit var card7: ImageView
    private lateinit var card8: ImageView
    private lateinit var card9: ImageView
    private lateinit var card10: ImageView
    private lateinit var card11: ImageView
    private lateinit var card12: ImageView

    //activity
    private var chronometerPausedTime: Long = -25000 //25 seconds
    private lateinit var cards: Array<ImageView>

    //view model
    private lateinit var viewModel: GameDeathclawViewModel

    //FUNCTIONS
    /**
     * This method is used to pause the game.
     */
    @SuppressLint("SetTextI18n")
    private fun pause() {
        //CHRONOMETER / MUSIC PAUSE
        chronometer.stop()
        chronometerPausedTime = SystemClock.elapsedRealtime() - chronometer.base
        viewModel.mp.pause()

        //CUSTOM TITLE
        val title = TextView(this)
        title.text = "PAUSE"
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25F)
        title.setTextColor(Color.parseColor("#1BFF80"))
        title.gravity = Gravity.CENTER
        title.typeface = Typeface.DEFAULT_BOLD
        title.setPadding(0, 30, 0, 20)

        //CUSTOM DIALOG
        val builder = AlertDialog.Builder(this, R.style.PauseDialogTheme).setCancelable(false)
        val view = layoutInflater.inflate(R.layout.custom_pause_dialog, null)
        builder.setView(view)
        builder.setCustomTitle(title)
        //resume button
        builder.setPositiveButton("resume") { _, _ ->
            viewModel.mp.start()
            chronometer.base = SystemClock.elapsedRealtime() - chronometerPausedTime
            chronometer.start()
        }
        //dialog creation and bg
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
        dialog.show()
    }

    /**
     * This method is used to end the game.
     */
    private fun endGame() {
        //The game ends if all cards are matched or the chronometer reaches 0.
        //The game time must be >= O because during the count down the time is considered as negative.
        if (viewModel.cards.all { c -> c.matched } || (SystemClock.elapsedRealtime() - chronometer.base) >= 0L) {
            viewModel.mp.stop()

            //INTENT
            val resultIntent = Intent(this, ResultActivity::class.java)
            //mode
            resultIntent.putExtra("gameMode", "Deathclaw")
            //result
            resultIntent.putExtra("score", (viewModel.cards.count { c -> c.matched }
                    * 10)/viewModel.counter.toLong())

            startActivity(resultIntent)
        }
    }

    /**
     * This method is used to flip a card.
     * @param card Card's ImageView
     * @param cardID Card's index on ViewModel Array
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    fun flipCard(card: ImageView, cardID: Int) {
        //View Model Card Flip
        card.setImageDrawable(getDrawable(viewModel.flipCard(card, cardID, moveCounter)))

        //HANDLER
        //This is used to delay the flip, so the user can see the second card before it flips back.
        Handler(Looper.getMainLooper()).postDelayed({
            viewModel.checkIfMatch(cards)
            endGame()
        },350)
    }

    //UPDATE UI
    /**
     * This method is used to update the game's UI.
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    fun updateUI() {
        card1.setImageDrawable(getDrawable(viewModel.cardState(0)))
        card2.setImageDrawable(getDrawable(viewModel.cardState(1)))
        card3.setImageDrawable(getDrawable(viewModel.cardState(2)))
        card4.setImageDrawable(getDrawable(viewModel.cardState(3)))
        card5.setImageDrawable(getDrawable(viewModel.cardState(4)))
        card6.setImageDrawable(getDrawable(viewModel.cardState(5)))
        card7.setImageDrawable(getDrawable(viewModel.cardState(6)))
        card8.setImageDrawable(getDrawable(viewModel.cardState(7)))
        card9.setImageDrawable(getDrawable(viewModel.cardState(8)))
        card10.setImageDrawable(getDrawable(viewModel.cardState(9)))
        card11.setImageDrawable(getDrawable(viewModel.cardState(10)))
        card12.setImageDrawable(getDrawable(viewModel.cardState(11)))
    }

    //ON CREATE
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_deathclaw)

        //VIEW MODEL
        viewModel = ViewModelProvider(this).get(GameDeathclawViewModel::class.java)

        //MUSIC
        if (!viewModel.isPlaying)
            viewModel.startMusic(this)
        else
            viewModel.restartMusic() //Used when the music was playing before an orientation change.

        //IDs
        pauseButton = findViewById(R.id.pause_button)
        chronometer = findViewById(R.id.chronometer)
        moveCounter = findViewById(R.id.movements)
        card1 = findViewById(R.id.card1)
        card2 = findViewById(R.id.card2)
        card3 = findViewById(R.id.card3)
        card4 = findViewById(R.id.card4)
        card5 = findViewById(R.id.card5)
        card6 = findViewById(R.id.card6)
        card7 = findViewById(R.id.card7)
        card8 = findViewById(R.id.card8)
        card9 = findViewById(R.id.card9)
        card10 = findViewById(R.id.card10)
        card11 = findViewById(R.id.card11)
        card12 = findViewById(R.id.card12)

        //MOVEMENTS COUNTER
        moveCounter.text = "MOVEMENTS: ${viewModel.counter}"

        //CARD LIST
        cards = arrayOf(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10,
            card11, card12)

        //CHRONOMETER
        chronometer.base = SystemClock.elapsedRealtime() - chronometerPausedTime
        chronometer.start()

        //ON CLICK
        //pause
        pauseButton.setOnClickListener {
            pause()
        }

        //cards
        card1.setOnClickListener(this)
        card2.setOnClickListener(this)
        card3.setOnClickListener(this)
        card4.setOnClickListener(this)
        card5.setOnClickListener(this)
        card6.setOnClickListener(this)
        card7.setOnClickListener(this)
        card8.setOnClickListener(this)
        card9.setOnClickListener(this)
        card10.setOnClickListener(this)
        card11.setOnClickListener(this)
        card12.setOnClickListener(this)

        //GAME OVER CHRONOMETER
        chronometer.setOnChronometerTickListener {
            endGame() //Checks if the game needs to end every time the chronometer changes.
        }

        //UI UPDATE
        updateUI()
    }

    //ON CLICK
    override fun onClick(view: View?) {
        when(view) {
            card1 -> flipCard(card1, 0)
            card2 -> flipCard(card2, 1)
            card3 -> flipCard(card3, 2)
            card4 -> flipCard(card4, 3)
            card5 -> flipCard(card5, 4)
            card6 -> flipCard(card6, 5)
            card7 -> flipCard(card7, 6)
            card8 -> flipCard(card8, 7)
            card9 -> flipCard(card9, 8)
            card10 -> flipCard(card10, 9)
            card11 -> flipCard(card11, 10)
            card12 -> flipCard(card12, 11)
        }
    }

    //STOP MUSIC AND CHRONOMETER
    override fun onBackPressed() {
        super.onBackPressed()
        viewModel.mp.stop()
    }

    override fun onPause() {
        super.onPause()
        chronometer.stop()
        chronometerPausedTime = SystemClock.elapsedRealtime() - chronometer.base
        viewModel.mp.pause()
    }

    //RESTART MUSIC AND CHRONOMETER
    override fun onResume() {
        super.onResume()
        chronometer.base = SystemClock.elapsedRealtime() - chronometerPausedTime
        chronometer.start()
        viewModel.mp.start()
    }

    //ORIENTATION CHANGE (CHRONOMETER)
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong("chronometerTime", SystemClock.elapsedRealtime() - chronometer.base)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.let {
            chronometerPausedTime = it.get("chronometerTime") as Long
        }
    }
}
