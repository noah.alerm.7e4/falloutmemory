package com.example.falloutmemory

import android.content.Context
import android.media.MediaPlayer
import android.widget.ImageView
import androidx.lifecycle.ViewModel

/**
 * This ViewModel is used to control the menu's audio during orientation changes.
 */
class MenuViewModel : ViewModel() {
    //ATTRIBUTES
    lateinit var mp: MediaPlayer
    var introTime = 0
    var isPlaying = true

    //FUNCTIONS
    /**
     * This method is used to start playing the intro.
     * @param context Menu's Context
     */
    fun startIntro(context: Context, view: ImageView) {
        mp = MediaPlayer.create(context, R.raw.war_never_changes_fallout_4_intro)
        mp.seekTo(introTime)
        mp.start()
        view.setImageResource(R.drawable.music_on)
        isPlaying = true
    }

    /**
     * This method is use to mute the intro.
     * @param view Muting ImageView
     */
    fun muteIntro(view: ImageView) {
        mp.stop()
        introTime = mp.currentPosition
        view.setImageResource(R.drawable.music_off)
        isPlaying = false
    }
}
