package com.example.falloutmemory

import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import android.util.Log

import java.io.IOException

import android.net.Uri

import java.io.FileOutputStream

import java.io.File
import androidx.core.content.FileProvider
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class ResultActivity : AppCompatActivity() {
    //VARIABLES
    //layout
    private lateinit var points: TextView
    private lateinit var playAgainButton: Button
    private lateinit var menuButton: Button
    private lateinit var shareButton: Button

    //activity
    private lateinit var mp: MediaPlayer
    private var hasBeenPlayed = false

    //ON CREATE
    @Suppress("LiftReturnOrAssignment")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        //INTENT EXTRAS
        val bundle: Bundle? = intent.extras
        val mode = bundle?.getString("gameMode")
        val score = bundle?.getLong("score")

        //IDs
        points = findViewById(R.id.points_text)
        playAgainButton = findViewById(R.id.play_again_button)
        menuButton = findViewById(R.id.menu_button)
        shareButton = findViewById(R.id.share_button)

        //AUDIO
        //This conditional is used to check if the audio has already been played once, so it
        // doesn't play every time the orientation of the screen changes.
        if (savedInstanceState == null || !savedInstanceState.getBoolean("audioState")) {
            mp = MediaPlayer.create(this, R.raw.result_sound)
            mp.seekTo(0)
            mp.start()
        }
        hasBeenPlayed = true

        //POINTS
        points.text = score.toString()
        YoYo.with(Techniques.Shake).duration(1000).playOn(points) //This animation is also played after orientation changes.

        //ON CLICK
        //play again button
        playAgainButton.setOnClickListener {
            val resultIntent = if (mode.equals("Normal"))
                Intent(this, GameNormalActivity::class.java)
            else if (mode.equals("Survival"))
                Intent(this, GameSurvivalActivity::class.java)
            else {
                Intent(this, GameDeathclawActivity::class.java)
            }

            startActivity(resultIntent)
        }

        //menu button
        menuButton.setOnClickListener {
            val resultIntent = Intent(this, MenuActivity::class.java)
            startActivity(resultIntent)
        }

        //share button
        shareButton.setOnClickListener {
            //SCREENSHOT
            //creating bitmap of current view
            val view = findViewById<View>(R.id.result_screen).rootView
            view.isDrawingCacheEnabled = true
            view.buildDrawingCache(true)
            val b = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false

            //saving bitmap in file
            val imagesFolder = File(cacheDir, "images")
            var uri: Uri? = null
            try {
                imagesFolder.mkdirs()
                val file = File(imagesFolder, "shared_image.png")
                val stream = FileOutputStream(file)

                b.compress(Bitmap.CompressFormat.PNG, 90, stream)

                stream.flush()
                stream.close()

                //creating uri for file
                uri = FileProvider.getUriForFile(this, "com.mydomain.fileprovider", file)
            } catch (e: IOException) {
                Log.d(TAG, "IOException while trying to write file for sharing: " + e.message)
            }

            //INTENT
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "image/png"
            //score screenshot
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri) //Adding screenshot uri to intent.
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //message
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "FALLOUT MEMORY SCORE")
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey, look at my score at Fallout Memory! " +
                    "Can you do it better than me?")
            //sharing
            startActivity(Intent.createChooser(shareIntent, "Share with..."))
        }
    }

    //ORIENTATION CHANGE
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("audioState", hasBeenPlayed)
    }
}
